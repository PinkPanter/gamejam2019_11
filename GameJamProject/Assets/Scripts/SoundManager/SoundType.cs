﻿using UnityEngine;

namespace FPSTestProject.Helpers.Runtime.SoundManager
{
    public enum SoundType
    {
        Shield = 100,

        Sword = 200,

        UIClose = 300,
        UIMove = 400,
        UIOpen = 500,
        UIWear = 600,

        TakeDamage = 700,

        Walk = 800,

        Sound = 900,

        MeeleHit = 1000,
        Healing = 1100,

        Door = 1200,
        Chest = 1300,

        Spell = 1400,

        Bow = 1500,
        Staff = 1600,
        PickUp = 1700,
        RageSound = 1800,
    }
}
