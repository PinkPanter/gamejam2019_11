﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
	[SerializeField]
	private Transform[] _startPositions;

	[SerializeField]
	private Camera _camera;

	[SerializeField]
	private Transform _mainPlayerObject;

	[SerializeField]
	private Transform _basePositionsContainer;

	[SerializeField]
	private Transform _directionObject;

	[SerializeField]
	private string DirectionDetectorName = "DirectionDetector";


	[SerializeField]
	private List<NavMeshAgent> _rangeAgents;

	[SerializeField]
	private List<NavMeshAgent> _meleAgents;

	[SerializeField]
	private List<Transform> _meleAgentsPositions;

	[SerializeField]
	private List<Transform> _rangedAgentsPositions;

	[SerializeField] private float _speed;

	[SerializeField] private float MeleeRange;
	[SerializeField] private float RangeRange;

	[SerializeField] private bool _meleeAttacking;

	[SerializeField] private UnitContainer _meleeContainer;
	[SerializeField] private RangedUnitContainer _rangeContainer;





	private void Start()
	{

		
		var units = Character.Characters[Character.CharType.Player];

		units.ForEach(a=>a.WeaponController.OnWeaponChange += ChangeUnitStage);
		var ranged = units.Where(u => u.IsRanged()).ToList();
		var melee = units.Where(u => !u.IsRanged()).ToList();
		_meleeContainer = new UnitContainer(this.transform, melee);
		_rangeContainer = new RangedUnitContainer(this.transform, ranged);

		_meleeContainer.AddAgents(_meleAgents);
		_meleeContainer.AddTargets(_meleAgentsPositions);

		_rangeContainer.AddAgents(_rangeAgents);
		_rangeContainer.AddTargets(_rangedAgentsPositions);
	}

	// Start is called before the first frame update

	private void Update()
	{
		FindDirection();
		MoveAgents();

	}



	public Vector3 GetDirection()
	{
		return _aimingDirection;
	}

	public void ChangeUnitStage(Character character)
	{
		if (character.IsRanged())
		{
			NavMeshAgent charNavMesh = character.GetComponent<NavMeshAgent>();
			if (_rangeContainer.Contains(charNavMesh, character))
			{
				Debug.LogError("is already in");
				return;
			}
			else
			{
				if (_meleAgents.Contains(charNavMesh))
					_meleAgents.Remove(charNavMesh);

				_meleeContainer.RemoveCharacterAndAgent(charNavMesh, character);
				_rangeContainer.AddAgentAndCharacter(charNavMesh, character);
			}
		}
		else
		{
			NavMeshAgent charNavMesh = character.GetComponent<NavMeshAgent>();
			if (_meleeContainer.Contains(charNavMesh, character))
			{
				Debug.LogError("is already in");
				return;
			}
			else
			{

				_rangeContainer.RemoveCharacterAndAgent(charNavMesh, character);
				_meleeContainer.AddAgentAndCharacter(charNavMesh, character);
			}
		}
	}

	private Vector3 _aimingDirection;

	private Vector3 MousePositionHit;
	private void FindDirection()
	{
		var mousePosition = Input.mousePosition;

		RaycastHit[] hits;
		var rayMouse = _camera.ScreenPointToRay(mousePosition);
		hits = Physics.RaycastAll(rayMouse.origin, rayMouse.direction, 100000);
		Debug.DrawRay(rayMouse.origin, rayMouse.direction * 10000, Color.black);
		MoveUnits();
		foreach (var raycastHit in hits)
		{
			if (raycastHit.transform.name == DirectionDetectorName)
			{
				MousePositionHit = raycastHit.point;
				var mouseRayHit = raycastHit.point;
				Debug.DrawLine(MousePositionHit, transform.position);

				//Debug.DrawRay(rayMouse.origin,mouseRayHit);

				var mouse0Y = new Vector3(mouseRayHit.x, 0, mouseRayHit.z);
				var trans0Y = new Vector3(transform.position.x, 0, transform.position.z);
				_aimingDirection = mouse0Y - trans0Y;
				_directionObject.forward = _aimingDirection;
			}
		}

		if (Input.GetKeyDown(KeyCode.Space))
		{
			Debug.Break();
		}
	}



	private void OnDrawGizmos()
	{
		Gizmos.DrawSphere(MousePositionHit, 1);
		Gizmos.color = Color.cyan;
		Gizmos.DrawWireSphere(this.transform.position, MeleeRange);
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(this.transform.position, RangeRange);
	}

	private void MoveAgents()
	{
		//MoveMelee();
		_meleeContainer.Move(_directionObject.forward);
		_rangeContainer.Move(_directionObject.forward);
	}

	private void MoveUnits()
	{
		var y = Input.GetAxis("Vertical") * _speed;
		var x = Input.GetAxis("Horizontal") * _speed;
		y *= Time.deltaTime;
		x *= Time.deltaTime;

		this.transform.position += new Vector3(x, 0, y);
	}

	public float GetRangeDistance()
	{
		return RangeRange;
	}

	public float GetMeleeDistance()
	{
		return MeleeRange;
	}

	private List<Character> _meleEnemies = null;
	private List<Character> _rangeEnemies = null;

	public void OnStartMeleeAttack(List<CharacterAndDistance> enemy)
	{
		//	Debug.LogError(enemy);
		_meleeContainer.SetEnemies(enemy);
		if (enemy != null)
			_meleeContainer.Unattach();
		else
			_meleeContainer.Attach();
	}


	public void OnStartRangeAttack(List<CharacterAndDistance> enemy)
	{
		_rangeContainer.SetEnemies(enemy);

		_rangeContainer.TryAttack();
	}


}


