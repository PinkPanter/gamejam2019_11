﻿namespace LifelongAdventure.Creatures.Data
{
    public enum Stat
    {
        MoveSpeed = 0,
        Health = 1,
        FrontResist = 2,
        BackResist = 3,
        SideResist = 4,
    }
    /*
    public enum WeaponStat
    {
        MinDamage = 0,
        MaxDamage = 1,
        Range = 2,
        Cooldown = 3,
        AnimationTime = 4,
        Angle = 5,
    }*/
}
