﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HasteEffect", menuName = "Effects/Haste")]
public class HasteEffectData : BaseEffectData
{
    public float tickTime;
    public float totalTime;
    public float speedMultiplier = 1.5f;


    
}

