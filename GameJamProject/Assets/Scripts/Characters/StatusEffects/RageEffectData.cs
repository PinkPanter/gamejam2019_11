﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "RageEffect", menuName = "Effects/Rage")]
public class RageEffectData : BaseEffectData
{
    public float tickTime;
    public float totalTime;
    public float additionaAS = 0.3f;
}
