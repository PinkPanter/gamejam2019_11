﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class PlayerPortrait : MonoBehaviour
    {
        public Image icon;

        public Slider helthBar;

        public Transform helthGrid;

        public Transform effectsGrid;
    }
}
